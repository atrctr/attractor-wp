    </div>

    <footer>
	    <p>&copy; <?php echo date('Y').' '; bloginfo('name'); ?></p><?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'fallback_cb'=>FALSE, 'container'=>FALSE, ) ); ?>
    </footer>

</div>

<?php wp_footer(); ?>
</body>
</html>
