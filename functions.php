<?php

register_nav_menus( array(
	'header-menu' => 'Header Menu',
	'footer-menu' => 'Footer Menu'
) );

add_theme_support('post-thumbnails');

if (function_exists('register_sidebar')) {
    register_sidebar(array(
		    'id'         => 'widgets',
		    'name'          => 'Widgets',
		    'before_title'  => '<h3>',
		    'after_title'   => '</h3>',
		    'before_widget' => '<div class="widget">',
		    'after_widget'  => '</div>',
	    ));
}


?>
