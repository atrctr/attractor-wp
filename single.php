<?php get_header(); ?> <!-- calls header.php -->
<?php if (have_posts()) : while (have_posts()) : the_post(); 

// check for a Featured Image and then assign it to a PHP variable for later use
if ( has_post_thumbnail() ) {
    $featured_image = get_the_post_thumbnail();
}

?>

	<article id="post-<?php the_ID(); ?>">
		
	<?php 
		if($featured_image) {
			$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			print '<div class="featured-img" style="background:url(\'' . $url . '\') center center no-repeat; background-size:cover">';
		}
	?>
		<?php the_title("\n<h1>","</h1>\n"); ?>		
	<?php if($featured_image) { print '</div>';} ?>
	        <ul class="infobox hlist">
			<li><?php the_date(); ?></li>
        	<?php if( comments_open() ): ?><li><a href='#comments'><?php print comments_number( 'No responses yet', '1 comment', '% comments' );  ?></a></li><?php endif; ?>
		    <li>in&nbsp;<?php the_category(', '); ?></li>
		    <li>by&nbsp;<?php the_author_posts_link(); ?></li>
		</ul>
	<?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?>
	
	</article>
		
	<?php # wp_link_pages(array('before' => '<p><b>Pages:</b> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
		
	<?php
	// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}
	?>
	
<?php endwhile; else: ?>

    <p>Sorry, no posts matched your criteria.</p>

<?php endif; ?>
			

<?php get_footer(); ?> <!-- calls footer.php -->
