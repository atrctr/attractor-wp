# atrctr Wordpress theme

Created by [Casper Kowalczyk](http://atrctr.com).

Based off [Duncan McKeans's](http://duncanmckean.com/) [mnml theme](http://mnml.sparkwoodand21.com/) - visit [s+21](http://sparkwoodand21.com/); originally licensed under CC BY-SA 3.0.


