<?php get_header(); ?> <!-- calls header.php -->

    <h1><?php single_cat_title(); ?></h1>
    <?php # if ( category_description() ) {echo "<div class='infobox'>".category_description()."</div>"; } ?>

    <?php get_template_part('article-list'); ?>
        
<?php get_footer(); ?> <!-- calls footer.php -->
