<?php get_header(); ?> <!-- calls header.php -->
	<article id="post-<?php the_ID(); ?>">
	
	<h1><?php the_title(); ?></h1>
	
	<ul class="infobox hlist">
		<li>Last updated <?php the_modified_date(); ?></li>
    </ul>
	
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
			<?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?>
			<?php wp_link_pages(array('before' => '<p><b>Pages:</b> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

	</article>
	    <?php
// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}
?>
        <?php endwhile; endif; ?>


<?php get_footer(); ?> <!-- calls footer.php -->
