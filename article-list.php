<article>
	
<div id='postlist'><!-- post list -->

<?php		
	$temp_query = $wp_query;
	while (have_posts()) : the_post();

	if ( has_post_thumbnail() ) {
    $featured_image = get_the_post_thumbnail();
}
	?>
	<a class="post-card<?php 
				if( has_post_thumbnail() ) { 
					echo " post-card-image"; 
				} else {
					echo " post-card-text";
				} 
				?>" <?php
			 if(has_post_thumbnail()) {
				$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
				print 'style="background:url(\'' . $url . '\') center center no-repeat; background-size:cover"';
			 } ?> href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
			
		<div class="label" >
			<?php the_title("<h2 class='title'>","</h2>"); ?>
			<div class="info"><?php the_date() ?><!-- &bull; <?php foreach (get_the_category() as $category) { echo $category->name." "; } ?>--></div>
			<div class='excerpt'><?php the_excerpt(); ?></div>
		</div>
	</a>
<?php endwhile; ?><!-- end loop -->

</div><!-- post list END -->
	
<!-- <div id='widget-area'>
	<?php //dynamic_sidebar( 'widgets' ); ?>
</div> -->

</article>
		
        <?php wp_reset_query(); ?>
