<?php get_header(); ?> <!-- calls header.php -->

	<div id="content">

	<h1><a href="/"><?php bloginfo('name'); ?></a> &raquo; <strong>404</strong></h1>
	<p>Good job, you just 404'd the website. You could as well go back to the <a href="<?php echo get_option('home'); ?>/">main page</a>.</p>
	</div>

<?php get_footer(); ?> <!-- calls footer.php -->
