<!DOCTYPE HTML> 
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />

<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> <?php } ?><?php wp_title(); ?></title>
	
<link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400i,700,700i|Montserrat:400,500" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_directory' ); ?>/reset.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<?php add_theme_support( 'automatic-feed-links' ); ?>

<!-- Always have wp_head() just before </head> otherwise plugins might break. -->
<?php wp_head(); ?>
</head>

<body>
<div id="container">
    <header>
        <a id='title' href="<?php echo home_url(); ?>">
            <div id='blog-title'><?php bloginfo('name'); ?></div>
            <div id='blog-tagline'><?php bloginfo('description'); ?></div>
        </a>
    
        <nav><?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?></nav>
    </header>

<div id="content">
